#### Before running, don't forget to:
*mvn clean install*
#### To run the Application: 
run JerseyApplication.java in src/main/java
#### For the Jersey IT Test: 
run JerseyIT.java in src/it/java
#### For the cucumber test:
run mvn clean install or test/java/cucumberit/CucumberIT.java
features are in test/resources/features

This application uses Jersey REST and Spring
Check package com.qualogy.configuration for their respective configuration classes
There are example of different techniques of autowiring:

* Autowiring through @Autowired
* Autowiring through @Resource
* Interface autowiring
* Class autowiring using @Component

There are some unit tests present to demonstrate how to use Mockito
to mock Spring components.

There is also a classic Controller setup demonstrated:
Each event that must be handled can be configured with a set of Actions
that must be performed