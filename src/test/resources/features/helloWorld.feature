Feature: HelloWorld can be called
  @stub
  Scenario: Client calls /hello
    When Header name with value Erwin
    When GET /hello
    Then the client receives a response of Erwin says Hello

  @stub
  Scenario: Keepalive
    When GET /keepalive
    Then the client receives a http status 200

  @stub
  Scenario: Post Keepalive
    When POST to /postAlive with body
    """
    {
      "name":"Erwin"
    }
    """
    Then the client receives http status 200 with body
    """
    {
      "message":"Erwin says Hello"
    }
    """
    When POST to /postAlive with file postBody.json as body
    Then the client receives http status 200 with file postResponse.json as body

  @stub
  Scenario: Get config for environment
    When GET /config
    Then the client receives a response of Erwin