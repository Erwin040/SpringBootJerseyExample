package cucumberit;

import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import uk.co.datumedge.hamcrest.json.SameJSONAs;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Definitions for writing feature files
 */
@Slf4j
public class CucumberDefinitions extends CucumberRoot {
    //This is the response entity we want to assert on
    private ResponseEntity<String> response;

    @When("^Header (.+) with value (.+)$")
    public void putHeaders(String field, String value) {
        template.getRestTemplate().setInterceptors(Collections.singletonList((request, body, execution) -> {
            request.getHeaders()
                    .add(field, value);
            return execution.execute(request, body);
        }));
    }

    @Then("^the client receives a response of (.+)$")
    public void the_client_receives_a_response(String result) {
        assertEquals(result,response.getBody());
    }

    @When("^GET (.+)$")
    public void the_client_calls_GET(String url) {
        response = template.getForEntity(url,String.class);
    }

    @Then("^the client receives a http status (\\d+)$")
    public void the_client_receives_http_status(int status){
        assertEquals(status,response.getStatusCodeValue());
    }

    @When("^POST to (.+) with body")
    public void post_to_url_with_body(String url, String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Object input = mapper.readValue(json,Object.class);
        response = template.postForEntity(url,input,String.class);
    }

    @When("^POST to (.+) with file (.+) as body")
    public void post_to_url_with_body_from_file(String url, String fileName) throws IOException {
        //Use ObjectMapper to convert the JSON input String to an Object
        ObjectMapper mapper = new ObjectMapper();
        Object input = mapper.readValue(new File("src/test/resources/"+fileName), Object.class);
        response = template.postForEntity(url, input, String.class);
    }

    @Then("^the client receives http status (\\d+) with body")
    public void the_client_receives_httpstatus_with_body(int status, String expectedOutput)  {
        //Use HamCrest to assertEquals JSONs as JUnit assertEquals or .equals() does not work
        assertThatPost(status, expectedOutput);
    }

    @Then("^the client receives http status (\\d+) with file (.+) as body")
    public void the_client_receives_httpstatus_with_body_from_file(int status, String fileName) throws IOException {
        StringBuilder expectedOutput = new StringBuilder();
        Files.lines(Paths.get("src/test/resources/"+fileName)).forEach(
                s -> expectedOutput.append(s).append("\n")
        );
        assertThatPost(status,expectedOutput.toString());
    }

    private void assertThatPost(int status, String expectedOutput) {
        assertThat(response.getBody(), SameJSONAs.sameJSONAs(expectedOutput));
        assertEquals(status,response.getStatusCodeValue());
    }

}
