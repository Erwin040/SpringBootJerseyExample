package com.qualogy.action;

import com.qualogy.dto.HelloWorldFlowDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GetConfigActionTest {
    public static final String HELLO_WORLD = "helloWorld";
    public static final String VALUE = "Erwin";
    private GetConfigAction test = new GetConfigAction();
    @Mock
    private HelloWorldFlowDto flowDtoMock;

    @Test
    public void testDefaultConfig(){
        ReflectionTestUtils.setField(test,HELLO_WORLD, VALUE);
        test.perform(flowDtoMock);
        verify(flowDtoMock).setConfig(VALUE);
    }
}
