package com.qualogy.resource;

import com.qualogy.controller.HelloWorldController;
import com.qualogy.data.PostRequest;
import com.qualogy.data.PostResponse;
import com.qualogy.dto.HelloWorldFlowDto;
import com.qualogy.service.MessageServiceAsInterface;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.ws.rs.core.Response;
import java.time.LocalDateTime;

import static com.qualogy.controller.AbstractController.Event.GET_CURRENT_DATETIME;
import static com.qualogy.controller.AbstractController.Event.SAY_HELLO;
import static junit.framework.TestCase.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class KeepAliveResourceTest {
    /**
     * Always use static constants as much as possible instead of local variables
     */
    private static final LocalDateTime LOCAL_DATE_TIME = LocalDateTime.of(2018,1,4,12,10,48);
    private static final String HELLO_WORLD = "Erwin says Hello";
    private static final String INPUT = "Erwin";
    @Mock
    private HelloWorldController helloWorldControllerMock;
    @Mock
    private MessageServiceAsInterface messageServiceAsInterfaceMock;
    @Mock
    private HelloWorldFlowDto flowDtoMock;

    @InjectMocks
    private KeepAliveResource test;

    /**
     * In here we specify mocks, instantiations and more that are common for all tests
     */
    @Before
    public void before() {
        when(helloWorldControllerMock.handleEvent(eq(GET_CURRENT_DATETIME),any(HelloWorldFlowDto.class))).thenReturn(flowDtoMock);
        when(helloWorldControllerMock.handleEvent(eq(SAY_HELLO),any(HelloWorldFlowDto.class))).thenAnswer(new Answer() {
            /*
            I know this can be a lambda but keep it like this for readability
            This block overrides the handleEvent with a return just like thenReturn, but also changes one of the objects
            in the arguments of the invoked mock. In this case, the second argument is a HelloWorldFlowDto object in which
            we call setHelloWorld to make sure this is present in the further execution of this test. The reason this
            is not a mock is because the HelloWorldFlowDto is created by the KeepAliveResource and not given as INPUT, so we
            cant replace that object with a mock
             */
            @Override
            public Object answer(InvocationOnMock invocationOnMock) {
                Object[] args = invocationOnMock.getArguments();
                HelloWorldFlowDto flowDto = (HelloWorldFlowDto) args[1];
                flowDto.setHelloWorld(HELLO_WORLD);
                return flowDto;
            }
        });
        when(flowDtoMock.getLocalDateTime()).thenReturn(LOCAL_DATE_TIME);
        when(messageServiceAsInterfaceMock.getMessage()).thenReturn(LOCAL_DATE_TIME.toString());
    }

    @Test
    public void keepAliveComponent() {
        assertEquals(LOCAL_DATE_TIME.toString(),test.keepAliveComponent());
    }

    @Test
    public void keepAliveInterface() {
        assertEquals(LOCAL_DATE_TIME.toString(),test.keepAliveInterface());
    }

    /**
     * When testing a Response object, use getEntity() and cast it to the Response object
     * First test the response status to be 200, then cast it's response. Otherwise,
     * an error will occur in casting PostResponse and you won't know what happened
     */
    @Test
    public void postAlive() {
        PostRequest postRequest = new PostRequest(INPUT);
        Response response = test.postAlive(postRequest);
        assertEquals(200,response.getStatus());
        PostResponse postResponse = (PostResponse) response.getEntity();
        assertEquals(HELLO_WORLD,postResponse.getMessage());
    }
}
