package com.qualogy.resource;

import com.qualogy.controller.AbstractController;
import com.qualogy.controller.HelloWorldController;
import com.qualogy.dto.HelloWorldFlowDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static com.qualogy.controller.AbstractController.Event.SAY_HELLO;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HelloWorldResourceTest {
    private static final String HELLO_WORLD = "Erwin says Hello";
    public static final String INPUT = "Erwin";
    /**
     * Mock the Autowired HelloWorldService
     */
    @Mock
    private HelloWorldController helloWorldControllerMock;
    /**
     * This is for returning a mock FlowDto from the controller
     */
    @Mock
    private HelloWorldFlowDto flowDtoMock;

    /**
     * Mock of the headers object
     */
    @Mock
    private HttpServletRequest httpServletRequest;

    /**
     * This annotation injects the Mocked Autowired components for HelloWorldResource into test
     */
    @InjectMocks
    private HelloWorldResource test = new HelloWorldResource();

    /**
     * Tell the test what to do when the mocked HelloWorldService is called
     * We are mocking the value of getHelloWorld() since it's implementation is outside this class
     */
    @Before
    public void setUp() {
        when(httpServletRequest.getHeader(eq("name"))).thenReturn(INPUT);
        when(helloWorldControllerMock.handleEvent(eq(SAY_HELLO),any(HelloWorldFlowDto.class))).thenReturn(flowDtoMock);
        when(flowDtoMock.getHelloWorld()).thenReturn(HELLO_WORLD);
    }

    /**
     * The test
     */
    @Test
    public void helloWorldTest() {
        String result = test.sayHello();
        assertEquals(HELLO_WORLD,result);
    }
}
