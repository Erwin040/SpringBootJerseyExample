package com.qualogy.controller;

import com.qualogy.action.Action;
import com.qualogy.dto.AbstractFlowDto;
import lombok.Getter;
import lombok.Setter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import javax.ws.rs.WebApplicationException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventHandlerTest {
    private static final String TEST = "test";
    @Mock
    private AbstractController abstractController;
    @Mock
    private ApplicationContext applicationContext;
    @Mock
    private AbstractController.EventConfig eventConfigMock;
    @Mock
    private AbstractController.ActionConfig actionConfigMock;

    @InjectMocks
    private EventHandler eventHandlerTest = new EventHandler(abstractController,applicationContext);

    @Before
    public void before() {
        when(abstractController.getEventConfig(AbstractController.Event.SAY_HELLO)).thenReturn(eventConfigMock);
        List<AbstractController.ActionConfig> actionConfigs = Arrays.asList(actionConfigMock);
        doReturn(TestAction.class).when(actionConfigMock).getAction();
        when(applicationContext.getBean(TestAction.class)).thenReturn(new TestAction());
        when(eventConfigMock.getActionConfigs()).thenReturn(actionConfigs);
    }

    @Test
    public void handleEvent() {
        MyFlowDto flowDto = new MyFlowDto();
        eventHandlerTest.handleEvent(AbstractController.Event.SAY_HELLO,flowDto);
        assertEquals(TEST,flowDto.getTest());
    }

    @Test(expected = IllegalStateException.class)
    public void beanNotFound() {
        when(applicationContext.getBean(TestAction.class)).thenReturn(null);
        eventHandlerTest.handleEvent(AbstractController.Event.SAY_HELLO, new MyFlowDto());
    }

    @Test(expected = WebApplicationException.class)
    public void exceptionInAction() {
        doReturn(ExceptionAction.class).when(actionConfigMock).getAction();
        when(applicationContext.getBean(ExceptionAction.class)).thenReturn(new ExceptionAction());
        eventHandlerTest.handleEvent(AbstractController.Event.SAY_HELLO, new MyFlowDto());
    }

    private final class TestAction implements Action<TestFlowDto> {
        @Override
        public void perform(TestFlowDto testFlowDto) {
            testFlowDto.setTest(TEST);
        }
    }
    private final class ExceptionAction implements Action<TestFlowDto> {
        @Override
        public void perform(TestFlowDto testFlowDto) {
            throw new WebApplicationException();
        }
    }
    private interface TestFlowDto {
        void setTest(String input);
    }
    @Setter
    @Getter
    private class MyFlowDto extends AbstractFlowDto implements TestFlowDto {
        private String test;
    }
}
