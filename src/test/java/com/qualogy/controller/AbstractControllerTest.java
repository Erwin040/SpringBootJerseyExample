package com.qualogy.controller;

import com.qualogy.action.Action;
import com.qualogy.dto.AbstractFlowDto;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;

import static com.qualogy.controller.AbstractController.Event.GET_CURRENT_DATETIME;
import static com.qualogy.controller.AbstractController.Event.SAY_HELLO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AbstractControllerTest {

    /**
     * To test the abstract implementation, we need an instance of a subclass of AbstractController
     * To prevent accessor issues, we use an inner class. Since all the methods we want to test are
     * at least protected, this will work
     */
    private TestController test = new TestController();

    @Test
    public void testController() {
        assertNotNull(test.getEventConfig(SAY_HELLO));
        assertEquals(2,test.getEventConfig(SAY_HELLO).getActionConfigs().size());
        test.getEventConfig(SAY_HELLO).getActionConfigs().forEach(actionConfig ->
                assertEquals(TestAction.class,actionConfig.getAction()));
    }

    @Test(expected = IllegalStateException.class)
    public void eventNotConfigured() {
        test.getEventConfig(GET_CURRENT_DATETIME);
    }

    /**
     * In the constructor of this TestController we specify the EventConfig we need for this test
     */
    private final class TestController extends AbstractController {
        TestController() {
            this.onEvent(SAY_HELLO).perform(TestAction.class).perform(TestAction.class);
        }
    }

    /**
     * This is a test action that we need as input to our TestController
     */
    private final class TestAction implements Action<TestDto> {

        @Override
        public void perform(TestDto testDto) {
        }
    }

    /**
     * The Action requires a <T extends AbstractFlowDto> but it doesn't need any variables
     */
    private final class TestDto extends AbstractFlowDto {}
}

