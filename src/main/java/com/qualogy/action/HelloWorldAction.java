package com.qualogy.action;

import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class HelloWorldAction implements Action<HelloWorldAction.HelloWorld> {
    private static final String RESPONSE = " says Hello";
    @Override
    public void perform(HelloWorld flowDto) {
        String output = new StringBuilder(flowDto.getName()).append(RESPONSE).toString();
        flowDto.setHelloWorld(output);
    }
    public interface HelloWorld {
        String getName();
        void setName(String input);
        String getHelloWorld();
        void setHelloWorld(String output);
    }
}
