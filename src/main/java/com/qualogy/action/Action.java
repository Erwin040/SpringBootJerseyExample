package com.qualogy.action;

/**
 * The generic interface for an Action that can be performed by the controller
 * Each action must have it's own interface with the methods that it requires
 * from the flowDto. The main flowDto then implements all interface from each
 * respective Action
 * @param <INPUT>
 */
public interface Action<INPUT> {
    void perform(INPUT input) throws Exception;
}
