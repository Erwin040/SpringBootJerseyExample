package com.qualogy.dto;

/**
 * The generic object used by the Controller as input. Do not put fields or implementations in here
 */
public abstract class AbstractFlowDto {
    //Abstract implementations might follow after SQL implementation
}
