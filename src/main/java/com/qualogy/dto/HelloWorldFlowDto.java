package com.qualogy.dto;

import com.qualogy.action.GetConfigAction;
import com.qualogy.action.HelloWorldAction;
import com.qualogy.action.LocalDateTimeAction;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class HelloWorldFlowDto extends AbstractFlowDto implements
        HelloWorldAction.HelloWorld,
        LocalDateTimeAction.KeepAlive,
        GetConfigAction.ConfigDto{
    private LocalDateTime localDateTime;
    private String name;
    private String helloWorld;
    private String config;
}
