package com.qualogy.data;

import com.qualogy.dto.AbstractFlowDto;

/**
 * This is a simple interface that expects an ApiResponse to define an injection of the FlowDto data into itself
 * The return value is a subclass of ApiResponse to allow this method to be called after new ApiResponse() and without first
 * declaring a new object of ApiResponse
 * @param <T>
 */
public interface ApiResponse<T extends AbstractFlowDto> {
    ApiResponse inject(T t);
}
