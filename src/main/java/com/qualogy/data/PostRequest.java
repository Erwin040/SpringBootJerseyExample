package com.qualogy.data;

import com.qualogy.dto.HelloWorldFlowDto;
import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
public class PostRequest implements ApiRequest<HelloWorldFlowDto> {
    @NotNull
    private String name;

    @Override
    public void extract(HelloWorldFlowDto flowDto) {
        flowDto.setName(this.name);
    }
}
