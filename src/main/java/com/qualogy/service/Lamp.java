package com.qualogy.service;


import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
public class Lamp {
    private Integer wattage;
    private String color;
    private Boolean on;

}
