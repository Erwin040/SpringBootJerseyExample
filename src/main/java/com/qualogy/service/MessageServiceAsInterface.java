package com.qualogy.service;

/**
 * Functional interface that can be implemented as a Bean elsewhere
 * See SpringConfiguration.class for the Bean
 */
public interface MessageServiceAsInterface {
    String getMessage();
}
