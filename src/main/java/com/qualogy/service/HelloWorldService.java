package com.qualogy.service;

import org.springframework.stereotype.Component;

/**
 * Creates a Bean with the default value: helloWorldService.
 * When using @Resource, specifying name is optional
 */
@Component
public class HelloWorldService {
    public String sayHello(String name){
        return new StringBuilder(name).append(" says Hello").toString();
    }
}
