package com.qualogy.controller;

import com.qualogy.action.Action;
import com.qualogy.dto.AbstractFlowDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import javax.ws.rs.WebApplicationException;
import java.util.Collection;

@Slf4j
public class EventHandler {
    private AbstractController abstractController;
    private ApplicationContext applicationContext;

    public EventHandler(final AbstractController abstractController, final ApplicationContext applicationContext) {
        this.abstractController = abstractController;
        this.applicationContext = applicationContext;
    }

    public <T extends AbstractFlowDto> T handleEvent(AbstractController.Event event, final T flowDto) {
        AbstractController.EventConfig eventConfig = abstractController.getEventConfig(event);

        Collection<AbstractController.ActionConfig> actionConfigs = eventConfig.getActionConfigs();
        try {
            for (AbstractController.ActionConfig actionConfig : actionConfigs) {
                performAction(flowDto, actionConfig);
            }
        } catch (IllegalStateException ise) {
            log.error("Controller was not configured correctly for event " + event + " message was: " + ise.getMessage());
            throw new IllegalStateException(ise.getCause());
        } catch (Exception e) {
            throw new WebApplicationException("An action resulted in an exception with message " + e.getMessage(), e.getCause());
        }
        return flowDto;
    }

    private <T extends AbstractFlowDto> void performAction(final T flowDto, final AbstractController.ActionConfig actionConfig) throws Exception {
        Action action = applicationContext.getBean(actionConfig.getAction());
        if (action == null) {
            throw new IllegalStateException("No bean is present for action " + actionConfig.getAction() + " perhaps you should annotate it with @Component?");
        }
        action.perform(flowDto);
    }
}
