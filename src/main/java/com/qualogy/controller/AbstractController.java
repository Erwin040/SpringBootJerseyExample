package com.qualogy.controller;

import com.qualogy.action.Action;

import java.util.*;

public abstract class AbstractController {
    public enum Event {
        SAY_HELLO,
        GET_CURRENT_DATETIME,
        GET_CONFIG
    }

    private Map<Event,EventConfig> eventConfigMap = new LinkedHashMap<>();

    protected EventConfig onEvent(final Event event) {
        if (!eventConfigMap.containsKey(event)){
            eventConfigMap.put(event,new EventConfig(event));
        }
        return eventConfigMap.get(event);
    }

    public EventConfig getEventConfig(final Event event) {
        EventConfig eventConfig = eventConfigMap.get(event);
        if (eventConfig == null) {
            throw new IllegalStateException("Event "+event+" not configured");
        }
        return eventConfig;
    }

    public class EventConfig {
        private Event event;
        private List<ActionConfig> actions = new ArrayList<>();

        private EventConfig(Event event) {
            this.event = event;
        }

        protected ActionConfig perform(Class<? extends Action> action) {
            ActionConfig actionConfig = new ActionConfig(this,action);
            actions.add(actionConfig);
            return actionConfig;
        }

        public Collection<ActionConfig> getActionConfigs() {
            return this.actions;
        }
    }

    public class ActionConfig {
        private EventConfig eventConfig;

        private Class<? extends Action> action;

        private ActionConfig(EventConfig eventConfig, Class<? extends Action> action){
            this.eventConfig = eventConfig;
            this.action = action;
        }

        public Class<? extends Action> getAction() {
            return action;
        }

        protected ActionConfig perform(Class<? extends Action> action) {
            return eventConfig.perform(action);
        }
    }


}
