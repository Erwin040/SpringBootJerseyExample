package com.qualogy.controller;

import com.qualogy.action.GetConfigAction;
import com.qualogy.action.HelloWorldAction;
import com.qualogy.action.LocalDateTimeAction;
import com.qualogy.dto.AbstractFlowDto;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import static com.qualogy.controller.AbstractController.Event.GET_CONFIG;
import static com.qualogy.controller.AbstractController.Event.GET_CURRENT_DATETIME;
import static com.qualogy.controller.AbstractController.Event.SAY_HELLO;

@Component
public class HelloWorldController extends AbstractController {
    @Resource
    private ApplicationContext applicationContext;

    private EventHandler eventHandler;

    /**
     * Uses PostConstruct because application context will be null before initialization finishes
     * Use:
     * this.onEvent(AbstractController.Event).perform(Action.class).perform(Action.class);
     * Each event can only be configured once, but each event can have unlimited actions
     */
    @PostConstruct
    public void init() {
        eventHandler = new EventHandler(this,applicationContext);
        //Configure all events
        this.onEvent(SAY_HELLO)
                .perform(HelloWorldAction.class);
        this.onEvent(GET_CURRENT_DATETIME)
                .perform(LocalDateTimeAction.class);
        this.onEvent(GET_CONFIG)
                .perform(GetConfigAction.class);

    }

    /**
     * Run Event event with input flowDto
     * @param event
     * @param <T> extends AbstractFlowDto
     * @return <T> extends flowDto
     */
    public <T extends AbstractFlowDto> T handleEvent(Event event, T flowDto) {
        eventHandler.handleEvent(event,flowDto);
        return flowDto;
    }

}
