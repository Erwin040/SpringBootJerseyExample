package com.qualogy.configuration;

import com.qualogy.service.Lamp;
import com.qualogy.service.MessageServiceAsInterface;
import org.springframework.context.annotation.*;

import java.time.LocalDateTime;

/**
 * Spring Configuration class. Make sure all packages containing @Component
 * annotations are inside ComponentScan list. Exception is the Jersey Resource
 * class
 */
@Configuration
@Import({})//Insert imports here for configuration that needs to be inherited, separate with comma's
@ComponentScan(basePackages = {"com.qualogy.service",
                               "com.qualogy.controller",
                               "com.qualogy.action"})//Insert the packages to scan
public class SpringConfiguration {
    /**
     * To use this bean in another class, use @Autowired
     * Or give the bean a name and use it with @Resource
     * @return
     */
    @Bean
    MessageServiceAsInterface getMessageService() {
        return () -> LocalDateTime.now().toString();
    }

    @Bean
    Lamp getPhilipsHueLamp (){
        Lamp lamp = new Lamp();
        lamp.setWattage(100);
        lamp.setColor("Blauw");
        lamp.setOn(true);
        return lamp;
    }

}
