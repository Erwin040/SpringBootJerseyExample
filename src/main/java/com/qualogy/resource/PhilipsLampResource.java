package com.qualogy.resource;


import com.qualogy.service.Lamp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Component
@Path("/")
public class PhilipsLampResource {

    @Autowired
    private Lamp lamp;

    @GET
    @Path("statusLamp")
    public String switchLamp(@QueryParam(value = "status") @NotNull String input) {
        Boolean status = validate(input);
        lamp.setOn(status);
        return lamp.getColor();
    }

    private static Boolean validate(String input) {
        Boolean status;
        switch (input) {
            case "true":
            case "True":
                status = true;
                break;
            case "false":
            case "False":
                status = false;
                break;
                default:
                    throw new RuntimeException("That's not a boolean");
        }
        return status;
    }
}
