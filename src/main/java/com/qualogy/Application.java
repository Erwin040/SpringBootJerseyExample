package com.qualogy;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * The main method of the Spring Boot application
 */
@SpringBootApplication
public class Application {
    //The Spring boot main method
    public static void main(String[] args){
        new SpringApplicationBuilder(Application.class)
                .build()
                .run(args);
    }

}
