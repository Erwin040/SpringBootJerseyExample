import com.qualogy.jersey.JerseyConfig;
import com.qualogy.configuration.SpringConfiguration;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.ws.rs.core.Application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JerseyIT extends JerseyTest {

    @Override
    protected Application configure() {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        return new JerseyConfig().property("contextConfig",context);
    }

    @Test
    public void testHelloWorld() {
        final String response = target("/hello").queryParam("name","Erwin").request().get(String.class);
        assertEquals("should return a message","Erwin says Hello",response);
    }

    @Test
    public void keepAlive() {
        final String response = target("/keepalive").request().get(String.class);
        assertNotNull(response);
    }
}
